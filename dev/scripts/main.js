$(document).ready(function() {
    $(".rslides").responsiveSlides({
        auto: true, // Boolean: Animate automatically, true or false
        speed: 500, // Integer: Speed of the transition, in milliseconds
        timeout: 5000, // Integer: Time between slide transitions, in milliseconds
        pager: false, // Boolean: Show pager, true or false
        nav: true, // Boolean: Show navigation, true or false
        random: false, // Boolean: Randomize the order of the slides, true or false
        pause: false, // Boolean: Pause on hover, true or false
        pauseControls: true, // Boolean: Pause when hovering controls, true or false
        prevText: "Previous", // String: Text for the "previous" button
        nextText: "Next", // String: Text for the "next" button
        maxwidth: "", // Integer: Max-width of the slideshow, in pixels
        navContainer: ".slider__nav", // Selector: Where controls should be appended to, default is after the 'ul'
        manualControls: "", // Selector: Declare custom pager navigation
        namespace: "rslides", // String: Change the default namespace used
        before: function() {}, // Function: Before callback
        after: function() {} // Function: After callback
    });
});

$(document).ready(function() {
    setTimeout(function() {
        $(".core-value--1").addClass("is-visible");
    }, 0);

    setTimeout(function() {
        $(".core-value--2").addClass("is-visible");
    }, 5000);

    setTimeout(function() {
        $(".core-value--3").addClass("is-visible");
    }, 10000);

    setTimeout(function() {
        $(".core-value--4").addClass("is-visible");
    }, 15000);

    setTimeout(function() {
        $(".core-value--5").addClass("is-visible");
    }, 20000);
});

$(document).ready(function() {
    $(".hamburger").on("click", function() {
        $(".site-nav--main").toggleClass("is-visible");
        $(this).toggleClass("is-active");
    });
});
